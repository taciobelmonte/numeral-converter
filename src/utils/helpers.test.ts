import { RomanNumerals } from './helpers';

describe('Numeral to Roman', () => {
    it('convert number 0 to Roman', () => {
        expect(RomanNumerals.toRoman(0)).toBe('');
    });

    it('convert number 1 to Roman', () => {
        expect(RomanNumerals.toRoman(1)).toBe('I');
    });

    it('convert number 2 to Roman', () => {
        expect(RomanNumerals.toRoman(2)).toBe('II');
    });

    it('convert number 4 to Roman', () => {
        expect(RomanNumerals.toRoman(4)).toBe('IV');
    });

    it('convert number 10 to Roman', () => {
        expect(RomanNumerals.toRoman(10)).toBe('X');
    });

    it('convert number 18 to Roman', () => {
        expect(RomanNumerals.toRoman(18)).toBe('XVIII');
    });

    it('convert number 27 to Roman', () => {
        expect(RomanNumerals.toRoman(27)).toBe('XXVII');
    });

    it('convert number 40 to Roman', () => {
        expect(RomanNumerals.toRoman(40)).toBe('XL');
    });

    it('convert number 50 to Roman', () => {
        expect(RomanNumerals.toRoman(50)).toBe('L');
    });

    it('convert number 60 to Roman', () => {
        expect(RomanNumerals.toRoman(60)).toBe('LX');
    });

    it('convert number 100 to Roman', () => {
        expect(RomanNumerals.toRoman(100)).toBe('C');
    });

    it('convert number 1000 to Roman', () => {
        expect(RomanNumerals.toRoman(1000)).toBe('M');
    });

    it('convert number 1900 to Roman', () => {
        expect(RomanNumerals.toRoman(1900)).toBe('MCM');
    });
});

describe('Roman to Numeral', () => {
    it('convert number I to 1', () => {
        expect(RomanNumerals.fromRoman('I')).toBe(1);
    });

    it('convert number II to 2', () => {
        expect(RomanNumerals.fromRoman('II')).toBe(2);
    });

    it('convert number C to 100', () => {
        expect(RomanNumerals.fromRoman('C')).toBe(100);
    });

    it('convert number M to 1000', () => {
        expect(RomanNumerals.fromRoman('M')).toBe(1000);
    });

    it('convert number MCM to 1900', () => {
        expect(RomanNumerals.fromRoman('MCM')).toBe(1900);
    });
});
