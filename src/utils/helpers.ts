import {
    romanBaseValues,
    integerBaseValues,
} from '../containers/NumeralConverter/data';
import { isNil, isEmpty } from 'ramda';

/**
 * Function to validate the number
 * @param number
 */
const validateNumber = (number: number) => {
    let error = false;
    let message = '';

    // Validate in case number is empty
    if (isNil(number)) error = true;

    // If number is not a string
    if (typeof number === 'string') {
        error = true;
        message = 'Please provide a number!';
    }

    return {
        error: error,
        message: message,
    };
};

// Export Roman Numerals
export const RomanNumerals = {
    toRoman: (number: number) => {
        let symbols: string[] = [];
        let remainder = 0;
        let quotient = 0;

        // Validate number
        const { error, message } = validateNumber(number);
        if (error) return message;

        // Check while number is greater than 0
        while (number > 0) {
            // Go through the based values defined and check what is the largest based value
            for (let i = 0; i < integerBaseValues.length; i++) {
                if (number >= integerBaseValues[i]) {
                    // Get the quotient and the remainder of the division
                    quotient = Math.floor(number / integerBaseValues[i]);
                    remainder = Math.floor(number % integerBaseValues[i]);

                    // Push the number of the quotient as symbol
                    for (let j = 0; j < quotient; j++)
                        symbols = [...symbols, romanBaseValues[i]];

                    number = remainder;
                }
            }
        }
        return symbols.join('');
    },
    fromRoman: (romanNumber: string) => {
        let result: number = 0;
        let roman: string = romanNumber.toUpperCase();

        // Validate in case number is empty
        if (isEmpty(roman)) return '';

        // Check if romanNumber
        if (typeof roman !== 'string') return 'Please provide a Roman number';

        for (let i in integerBaseValues) {
            while (roman.indexOf(romanBaseValues[i]) === 0) {
                result += integerBaseValues[i];
                roman = roman.replace(romanBaseValues[i], '');
            }
        }
        return result;
    },
};
