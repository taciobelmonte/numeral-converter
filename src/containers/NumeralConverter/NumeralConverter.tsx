import React, { useState } from 'react';
import * as S from './NumeralConverter.styled';
import { SharedStyle } from './../../shared';
import { Page } from './../../components';
import { RomanNumerals } from './../..//utils';

const NumeralConverter = () => {
    const [numeral, setNumeral] = useState(0);
    const [romanNumber, setRomanNumber] = useState('');

    return (
        <Page>
            <SharedStyle.Section>
                <SharedStyle.H1>Numeral to Roman Converter</SharedStyle.H1>
                <br />
                <SharedStyle.Paragraph
                    variant="info"
                    lineHeight={1.5}
                    color="#113f9a"
                    fontSize={18}
                >
                    Please, insert the number you wish to convert to roman
                    number in the field below. <br />
                    The result will be the number provided in Roman format.
                </SharedStyle.Paragraph>
                <S.Input
                    type="number"
                    onChange={e => setNumeral(parseInt(e.target.value))}
                />
                <SharedStyle.Paragraph variant="error">
                    {RomanNumerals.toRoman(numeral)}
                </SharedStyle.Paragraph>
            </SharedStyle.Section>
            <SharedStyle.Section>
                <SharedStyle.H1>Roman to Numeral Converter</SharedStyle.H1>
                <br />
                <SharedStyle.Paragraph
                    variant="info"
                    lineHeight={1.5}
                    color="#113f9a"
                    fontSize={18}
                >
                    Please, insert the roman number you wish to convert in the
                    field below. <br />
                    The result will be the numeral.
                </SharedStyle.Paragraph>
                <S.Input
                    type="text"
                    onChange={e => setRomanNumber(e.target.value)}
                />
                <SharedStyle.Paragraph variant="error">
                    {RomanNumerals.fromRoman(romanNumber)}
                </SharedStyle.Paragraph>
            </SharedStyle.Section>
        </Page>
    );
};

export default NumeralConverter;
