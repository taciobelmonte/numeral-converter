import styled from 'styled-components';

export const Input = styled.input`
    border: 1px solid #eee;
    border-radius: 80px;
    font-size: 55px;
    outline: 0;
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
    text-align: center;
    color: #bbb;
    font-weight: bold;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;

    &:focus {
        background: #eee;
        color: #f3cc00;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
    }
`;

export const Result = styled.p`
    color: #ff0000;
    min-height: 35px;
    font-size: 18px;
    word-break: break-all;
`;
