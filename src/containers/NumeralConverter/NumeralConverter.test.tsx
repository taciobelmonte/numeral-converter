import React from 'react';
import { render, screen } from '@testing-library/react';
import NumeralConverter from './NumeralConverter';
import { BrowserRouter as Router } from 'react-router-dom';

describe('Numeral Converter Component', () => {
    it('render NumeralConverter without crashing', () => {
        render(
            <Router>
                <NumeralConverter />
            </Router>,
        );
    });

    it('should have sections titles', () => {
        render(
            <Router>
                <NumeralConverter />
            </Router>,
        );
        screen.getByText('Numeral to Roman Converter');
        screen.getByText('Roman to Numeral Converter');
    });
});
