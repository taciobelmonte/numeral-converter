import React from 'react';
import ReactDOM from 'react-dom';
import NumeralConverter from './containers/NumeralConverter/NumeralConverter';
import { About } from './components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './assets/global.css';

ReactDOM.render(
    <React.StrictMode>
        <Router>
            <Switch>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/">
                    <NumeralConverter />
                </Route>
            </Switch>
        </Router>
    </React.StrictMode>,
    document.getElementById('root'),
);
