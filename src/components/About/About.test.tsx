import React from 'react';
import { render } from '@testing-library/react';
import About from './About';
import { BrowserRouter as Router } from 'react-router-dom';

test('render About', () => {
    render(
        <Router>
            <About />
        </Router>,
    );
});
