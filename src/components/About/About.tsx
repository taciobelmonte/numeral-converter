import { useEffect, useState } from 'react';
import { SharedStyle } from './../../shared';
import { Page } from './../../components';
import { IQuotes } from './interfaces';
import { Loader } from './About.styled';

const About = () => {
    const [quotes, setQuotes] = useState<IQuotes>({
        text: '',
        author: '',
    });
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        // Set is loading to true
        setIsLoading(true);

        // Fetch data
        fetch('https://type.fit/api/quotes')
            .then(response => response.ok && response.json())
            .then(data => {
                // Add data to state and change isLoading to false
                setQuotes(data[Math.floor(Math.random() * 200)]);
                setIsLoading(false);
            });
    }, []);

    return (
        <Page>
            <SharedStyle.Section>
                <SharedStyle.H1>About</SharedStyle.H1>
                <br />
                {isLoading ? (
                    <Loader />
                ) : (
                    quotes && (
                        <>
                            <p>{quotes.text}</p>
                            <p>
                                <i>{quotes.author}</i>
                            </p>
                        </>
                    )
                )}
            </SharedStyle.Section>
        </Page>
    );
};

export default About;
