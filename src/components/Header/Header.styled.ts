import styled from 'styled-components';

export const Logo = styled.div`
    background: #2e6678;
    margin: 0 auto;
    color: #fff;
    padding: 10px;
    font-size: 28px;
    font-weight: bold;
`;
