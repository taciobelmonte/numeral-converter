import React from 'react';
import { Logo } from './Header.styled';
import { SharedStyle } from './../../shared';

const Header = () => (
    <SharedStyle.Container>
        <Logo>Numeral Converter</Logo>
    </SharedStyle.Container>
);

export default Header;
