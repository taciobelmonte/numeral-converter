import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from './Header';
import { BrowserRouter as Router } from 'react-router-dom';

test('render Header', () => {
    render(
        <Router>
            <Header />
        </Router>,
    );

    // Expect to have logo
    screen.getByText('Numeral Converter');
});
