import React from 'react';
import { render, screen } from '@testing-library/react';
import Footer from './Footer';
import { BrowserRouter as Router } from 'react-router-dom';

describe('renders Footer', () => {
    render(
        <Router>
            <Footer />
        </Router>,
    );

    it('should have two links', async () => {
        const items = await screen.findAllByRole('link');
        expect(items).toHaveLength(2);
    });
});
