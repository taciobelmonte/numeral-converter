import React from 'react';
import { NavLink } from 'react-router-dom';
import { Container } from './Footer.styled';

const Footer = () => {
    return (
        <Container>
            <ul>
                <li>
                    <NavLink role="link" exact activeClassName="active" to="/">
                        Home
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        role="link"
                        exact
                        activeClassName="active"
                        to="/about"
                    >
                        About
                    </NavLink>
                </li>
            </ul>
            <p>
                {new Date().getFullYear()} - All rights reserved - Tácio
                Belmonte
            </p>
        </Container>
    );
};

export default Footer;
