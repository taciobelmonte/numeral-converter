import styled from 'styled-components';

export const Container = styled.div`
    background: #282c34;
    padding: 20px 0;
    color: #fff;

    a {
        color: #fff;
        text-decoration: none;
    }

    a:hover,
    a.active {
        text-decoration: underline;
    }

    ul {
        list-style: none;
        padding: 0;

        li {
            margin: 20px 0;
        }
    }
`;
