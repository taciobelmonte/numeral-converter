import React from 'react';
import * as S from './Page.styled';
import { Header, Footer } from './../../../components';

const Page: React.FC = ({ children }) => {
    return (
        <S.PageContainer>
            <Header />
            <S.Content>{children}</S.Content>
            <Footer />
        </S.PageContainer>
    );
};

export default Page;
