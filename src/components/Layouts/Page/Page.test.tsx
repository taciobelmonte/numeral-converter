import React from 'react';
import { render } from '@testing-library/react';
import Page from './Page';
import { BrowserRouter as Router } from 'react-router-dom';

test('render Page', () => {
    render(
        <Router>
            <Page />
        </Router>,
    );
});
