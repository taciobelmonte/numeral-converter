import styled from 'styled-components';

export const PageContainer = styled.div`
    font-family: 'Lato', sans-serif;
    text-align: center;
`;

export const Content = styled.div`
    min-height: calc(100vh - 300px);
    padding: 40px;
    display: flex;
    justify-content: center;
    flex-direction: column;

    @media (min-width: 992px) {
        flex-direction: row;
    }
`;
