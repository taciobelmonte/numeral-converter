import styled from 'styled-components';
import { IParagraph, Variant } from './types';

export const H1 = styled.h1`
    font-size: 24px;
    margin: 0;
`;

export const Paragraph = styled.p<IParagraph>`
    font-size: ${({ variant, fontSize }) =>
        variant === Variant.Error || variant === Variant.Success
            ? '24px'
            : variant === Variant.Info
            ? `${fontSize}px`
            : '16px'};
    min-height: 30px;
    color: ${({ variant }) =>
        variant === Variant.Error
            ? '#ff0000'
            : variant === Variant.Info
            ? '#113f9a'
            : variant === Variant.Success
            ? '#228B22'
            : '#eee'};
    line-height: ${({ lineHeight }) =>
        lineHeight ? `${lineHeight}em` : '1.2em'};
`;

export const Container = styled.div`
    box-shadow: 0 0 10px #eee;
    padding: 0;
`;

export const Section = styled.section`
    padding: 40px 0;
    display: flex;
    justify-content: center;
    flex-direction: column;

    @media (min-width: 992px) {
        padding: 20px;
    }
`;
