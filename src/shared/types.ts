export interface IParagraph {
    lineHeight?: number;
    variant?: string;
    fontSize?: number;
}

export enum Variant {
    Error = 'error',
    Info = 'info',
    Success = 'success',
}
