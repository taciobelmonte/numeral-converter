# Numeral Converter App

Numeral Converter App is an application specifically designed to convert the numeral provided to any other format.

The release version only includes the convertion from numeral to Roman format and vice-versa.

The architecture designed and developed for the project allows to scale and extend the resources of the application simply and quickly.

## Technologies

Numeral Converter App was developed by using ReactJS/TypeScript.

For CSS, this project utilizes Styled Components as CSS in JS.

To start the project, you can follow the instructions below.

## How to get started

Firstly, you need to install NPM.

After NPM installed and running, you will need to setup this app. See instructions below:

1 - Install all project dependencies with `npm install`

2 - Start the development server with `npm start`

It's done! You should have Numeral Converter App running straight way... You just need to open `http://localhost:3000` in your browser if it doesn't start properly.

## Diving into project conception

In addition, this project has utilized React Router in order to create two Routes:

1 - The mains route (/)

2 - The about route (/about)

## NumeralConverter

Main component of the application.

## About

Component that displays the About page.

## Project Structure

```bash
├── README.md
├── package.json
├── public
│   └── index.html
    └── manifest.json
└── src
    ├── assets
        ├── styles
            ├── global.css
    ├── components
        ├── About
            ├── About.tsx
            ├── About.styled.ts
            ├── About.test.js
        ├── Footer
            ├── Footer.tsx
            ├── Footer.styled.ts
            ├── Footer.test.js
        ├── Header
            ├── Header.tsx
            ├── Header.styled.ts
            ├── Header.test.js
        ├── Layouts
            ├── Page
                ├── Page.ts
                ├── Page.styled.js
                ├── Page.test.js
    ├── containers
        ├── NumeralConverter
            ├── data.js
            ├── NumeralConverter.tsx
            ├── NumeralConverter.styled.js
            ├── NumeralConveter.test.js
    ├── shared
        ├── index.ts
        ├── styles.ts
        ├── types.ts
    ├── utils
        ├── helpers.js
        ├── helpers.test.js
    └── index.js
```

## Architecture

The UI was separated by components, modularizing the html, avoiding repeated and unnecessary code.

This model allows the application to be simple, scalable and powerful when developing new functionalities.

## Limitations

1 - Only Roman conversion is enabled by now. Futures conversions can be easily added.

### Developed by Tácio Belmonte
